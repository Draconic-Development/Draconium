## Draconium
A highly customizable Discord / Powercord theme developed by Ryox#4416 with inspiration and help from FrostedGlass, ClearVision and CreArts. Dark theme is required.

- - -
### Powercord Install 
This is written with the assumption that powercord is installed in it's default location. If you chose to install it elsewhere replace step 2's command with powercords install location.

1. Open a CMD, Terminal or Powershell window and execute the following commands:
2. `cd C:\Users\%USERNAME%\powercord\src\Powercord\themes\`
3. `git clone https://gitlab.com/Draconic-Development/Draconium`
4. Close the command window and return to Discord. 
5. Open your user settings in the bottom left and navigate to "Themes".
6. Click the three vertical dots on the right of the page and select "Load missing themes". The Theme should automatically apply.

Feel free to visit our Discord if you are having any trouble getting Draconium installed. [Discord](https://discord.gg/X4DKMsurtE)
- - -
### Preview

Server chat + User popout
![Server Chat](https://themes.dragonsguild.games/Draconium4/preview/Chat-UserPopup.png)

Optional Horizontal Server List
![HSL Server List](https://themes.dragonsguild.games/Draconium4/preview/HSL.png)

User modal
![User Modal](https://themes.dragonsguild.games/Draconium4/preview/User%20Modal.png)

User Settings
![User Settings](https://themes.dragonsguild.games/Draconium4/preview/User%20Settings.png)

## License

See the [LICENSE](https://themes.dragonsguild.games/Draconium4/LICENSE.md) file for license rights and limitations (MIT).
